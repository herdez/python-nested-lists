## Segunda calificación más baja en lista nesteada

Es importante documentarse acerca del uso de las funciones `lambda` para ayudarte a obtener el valor mínimo de una `nested list`, asimismo documentarse sobre como ordenar una lista en Python.

Define la función nesteada `second_grade` que tomará un número `num` del usuario como primera entrada. Este número le indicará al programa el número de alumnos y calificaciones que el usuario escribirá en la terminal. 

Los nombres y calificaciones capturados por el usuario serán insertados en una `nested list`:


```python
"""Ejemplo de lista con 3 estudiantes y su calificación"""

students = [["Robert", 23.5], ["Karla", 23.5], ["Rodrigo", 23]]
```

El objetivo es obtener la segunda calificación más baja de la lista. Si hay dos o más estudiantes con la segunda calificación más baja hay que ordenar sus nombres alfabéticamente.

```python
['Karla', 'Robert']
```

Usa los tres ejemplos de entradas del usuario y pruebas que se presentan en el `driver code` para verificar si tu programa pasa las pruebas correspondientes:

```python
"""second_grade function"""

def second_grade():
    """Obtiene la segunda calificación más baja de la lista."""
    
    #se obtienen las entradas del usuario
    
    
    #se obtiene la mínima calificación
    def minimum(array):
        """Obtiene la mínima calificación de la lista."""
       


    #se obtienen los elementos con calificaciones diferentes a la mínima
   

    #se obtienen los segundas calificaciones mínimas
   

    #se retorna la lista de nombres ordenada alfabéticamente
    


#driver code

"""Ejemplo 1 de entradas de usuario"""

3
Robert
23.5
Karla
23.5
Rodrigo
23

print(second_grade() == ['Karla', 'Robert'])
# True

"""Ejemplo 2 de entradas de usuario"""

5
Faby
35.6
Gaby
35
Lourdes
35.6
Magy
47
Pepe
50

print(second_grade() == ['Faby', 'Lourdes'])
# True

"""Ejemplo 3 de entradas de usuario"""

3
Robert
23.5
Karla
50.4
Rodrigo
23

print(second_grade() == ['Robert'])
# True
```

> Tips:

- Usa las siguientes referencias para saber como obtener el mínimo y máximo en una `nested list` en Python: 
	- [Python max function using 'key' and lambda expression](https://stackoverflow.com/questions/18296755/python-max-function-using-key-and-lambda-expression).
	- [Python: usuage of lambda function in sort, max, min, map, filter, reduce](http://tech.7starsea.com/post/311).

